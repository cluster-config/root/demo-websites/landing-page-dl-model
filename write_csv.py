import csv
import json
from glob import glob

if __name__ == '__main__':
    with open('./data/pointer_dataset.csv', 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(['image', 'keypoint_x', 'keypoint_y', 'is_closed'])

        for file in glob("./data/open/*.json"):
            with open(file) as f:
                json_dict: dict = json.load(f)
                path = 'open/' + json_dict['imagePath']
                width = json_dict['shapes'][0]['points'][0][0]
                height = json_dict['shapes'][0]['points'][0][1]
                csv_writer.writerow([path, width, height, 0])

        for file in glob("./data/closed/*.json"):
            with open(file) as f:
                json_dict: dict = json.load(f)
                path = 'closed/' + json_dict['imagePath']
                width = json_dict['shapes'][0]['points'][0][0]
                height = json_dict['shapes'][0]['points'][0][1]
                csv_writer.writerow([path, width, height, 1])
