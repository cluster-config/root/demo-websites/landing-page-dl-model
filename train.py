import os

import tensorflow as tf
from tensorflow.python.keras.losses import mean_squared_error
from tensorflow.python.ops.nn_ops import softmax_cross_entropy_with_logits_v2

from load_data import create_pointer_dataset
from model_v0 import PointerModel


def loss(target_y, pred_y):
    target_label = target_y[:, 2]
    pred_label = pred_y[:, 2:]

    mask = tf.cast(target_label != 2, dtype=tf.float32)
    target_label = tf.one_hot(tf.cast(target_label, dtype=tf.uint8), depth=3)

    target_keypoint = target_y[:, :2]
    pred_keypoints = pred_y[:, :2]

    label_loss = softmax_cross_entropy_with_logits_v2(labels=target_label, logits=pred_label)
    keypoint_loss = mean_squared_error(target_keypoint, pred_keypoints) * mask
    return keypoint_loss + label_loss


if __name__ == '__main__':
    input_shape = (224, 224, 3)

    pointer_dataset = create_pointer_dataset(input_shape)

    split_at = int(len(pointer_dataset) * 0.8)

    train_dataset = pointer_dataset.take(split_at).shuffle(1024).batch(64)
    val_dataset = pointer_dataset.skip(split_at).batch(64)

    initial_learning_rate = 1e-4
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate,
        decay_steps=100000,
        decay_rate=0.96,
        staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)

    model = PointerModel(input_shape)

    model.predict(tf.zeros((1, *input_shape), dtype=tf.float32))

    checkpoint_path = "./checkpoints/cp.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     verbose=1,
                                                     save_best_only=True)

    model.compile(run_eagerly=True, optimizer=optimizer, loss=loss)
    model.fit(
        train_dataset,
        epochs=100,
        validation_data=val_dataset,
        callbacks=[cp_callback]
    )
    model.load_weights("./checkpoints/cp.ckpt")
    model.save("./saved_model/")
