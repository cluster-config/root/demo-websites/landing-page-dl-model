from abc import ABC

import tensorflow as tf
from tensorflow.python.keras import Input
from tensorflow.python.keras.layers import Dense, GlobalAveragePooling2D, Dropout
from tensorflow.python.keras.models import Model


class PointerModel(Model, ABC):
    def __init__(self, image_shape, **kwargs):
        super(PointerModel, self).__init__(**kwargs)
        self.image_shape = image_shape

        x = Input(image_shape)
        self.base_model = tf.keras.applications.MobileNetV2(
            include_top=False, weights='imagenet', input_shape=image_shape
        )
        self.global_avg_pool = GlobalAveragePooling2D()
        self.dense_1 = Dense(128, activation='relu')
        self.dropout = Dropout(0.1)
        self.dense_2 = Dense(5)  # keypoint_x, keypoint_y, label_open, label_closed, label_keypoint_not_in_image

        self.build((None, *image_shape))
        self.inputs = [x]
        self.outputs = self.call(x)

    def call(self, x, training=False, mask=None):
        x = self.base_model(x)
        x = self.global_avg_pool(x)
        x = self.dense_1(x)
        if self.trainable:
            x = self.dropout(x)
        x = self.dense_2(x)
        return x


if __name__ == '__main__':
    input_shape = (224, 224, 3)

    model = PointerModel(input_shape)
    model.summary()
    print(model(tf.zeros((1, *input_shape)))[0])
