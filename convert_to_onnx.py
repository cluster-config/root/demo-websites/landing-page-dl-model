import tensorflow as tf

from model_v0 import PointerModel

input_shape = (224, 224, 3)
model = PointerModel(input_shape)

model.predict(tf.zeros((1, *input_shape), dtype=tf.float32))

model.load_weights("./checkpoints/cp.ckpt")
model.save("./saved_model/")
