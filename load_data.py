from functools import partial

import albumentations
import albumentations.augmentations.transforms as alb
import pandas as pd
import tensorflow as tf
from albumentations import *
from matplotlib import pyplot as plt
from tensorflow.python.data.ops.dataset_ops import AUTOTUNE
from tensorflow_datasets.core import benchmark


def read_data(inp):
    image = tf.io.read_file("./data/" + inp['image'])
    image = tf.image.decode_png(image, channels=3)

    keypoint_x = tf.cast(inp['keypoint_x'], tf.float32)
    keypoint_y = tf.cast(inp['keypoint_y'], tf.float32)

    label = tf.cast(inp['is_closed'], tf.float32)
    return image, keypoint_x, keypoint_y, label


transforms = Compose([
    alb.ShiftScaleRotate(shift_limit=.25, border_mode=cv2.BORDER_REPLICATE),
    alb.CenterCrop(1024, 1024),
    alb.Resize(224, 224),
    alb.HorizontalFlip(),
    alb.VerticalFlip(),
    alb.MotionBlur(),
    alb.HueSaturationValue(),
    alb.RGBShift(),
    alb.ISONoise(),
], keypoint_params=albumentations.KeypointParams(format='yx', remove_invisible=False))


def augment(image, keypoint_x, keypoint_y, image_shape):
    aug_data = transforms(image=image, keypoints=[(keypoint_y, keypoint_x)])
    image = aug_data["image"]
    image = tf.cast(image, tf.float32)
    image = image / 255.
    keypoints = aug_data["keypoints"]
    keypoints = tf.cast(keypoints, dtype=tf.float32)
    keypoint_x = keypoints[0, 1] / len(image[0])
    keypoint_y = keypoints[0, 0] / len(image)
    return image, keypoint_x, keypoint_y


def process_data(image, keypoint_x, keypoint_y, label, image_shape):
    image, keypoint_y, keypoint_x = tf.numpy_function(func=augment, inp=[image, keypoint_x, keypoint_y, image_shape],
                                                      Tout=[tf.float32, tf.float32, tf.float32])
    image.set_shape(image_shape)
    keypoint_y.set_shape([])
    keypoint_x.set_shape([])
    label.set_shape([])
    keypoint_outside_image = tf.reduce_any([
        tf.less(keypoint_x, 0),
        tf.greater(keypoint_x, 1),
        tf.less(keypoint_y, 0),
        tf.greater(keypoint_y, 1)
    ])
    label = tf.cond(
        keypoint_outside_image,
        lambda: tf.cast(2, tf.float32),
        lambda: label
    )
    return image, tf.convert_to_tensor([keypoint_x, keypoint_y, label], dtype=tf.float32)


def create_pointer_dataset(image_shape=(527, 256, 3)):
    df = pd.read_csv("./data/pointer_dataset.csv")
    dataset = tf.data.Dataset.from_tensor_slices(dict(df))
    dataset = dataset.map(read_data, AUTOTUNE).prefetch(AUTOTUNE)
    dataset = dataset.map(partial(process_data, image_shape=image_shape), AUTOTUNE).prefetch(AUTOTUNE)
    return dataset


def vis_keypoints(image, keypoint_x, keypoint_y, diameter=5):
    image = image.numpy().copy()
    keypoint_x = int(keypoint_x * image.shape[0])
    keypoint_y = int(keypoint_y * image.shape[1])
    print(keypoint_x, keypoint_y)
    cv2.circle(image, (keypoint_y, keypoint_x), diameter, (0, 255, 0), -1)
    plt.figure(figsize=(8, 8))
    plt.axis('off')
    plt.imshow(image)
    plt.show()


if __name__ == '__main__':
    input_shape = (224, 224, 3)
    pointer_dataset = create_pointer_dataset(input_shape)
    print(pointer_dataset)
    for image, (keypoint_x, keypoint_y, label) in pointer_dataset:
        vis_keypoints(image, keypoint_x, keypoint_y)
    benchmark(pointer_dataset, num_iter=10)
